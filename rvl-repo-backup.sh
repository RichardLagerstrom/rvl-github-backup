#!/bin/bash

# Header stuff
PASSWORD_ENCODED=Your-github-password-base64-encoded

# Define some vars
GITHUB_USERNAME=richvalen
GITHUB_PASSWORD="$(echo $PASSWORD_ENCODED | base64 -d)"
TOKEN_FILE=data/token
JSON_FILE=data/authorizations_json.tmp
REPO_LIST_FILE=data/repo_list_json.tmp
BRANCHES_FILE=data/branches.tmp
BACKUP_DIR=data/backup/
GIT=/usr/local/git/bin/git

# Create backup dir
if [ ! -d $BACKUP_DIR ];
then
	echo "Missing BACKUP dir"
	echo "Creating $BACKUP_DIR ..."
	$(mkdir -p $BACKUP_DIR)
	$(chmod 755 $BACKUP_DIR)
fi

# Create required files
if [ ! -f $JSON_FILE ];
then
	echo "Missing temporary file for JSON"
	echo "Creating $JSON_FILE ..."
	$(touch $JSON_FILE)
	$(chmod 755 $JSON_FILE)
fi

# Create repo list file
if [ ! -f $REPO_LIST_FILE ];
then
	echo "Missing temporary file for REPO_LIST"
	echo "Creating $REPO_LIST_FILE ..."
	$(touch $REPO_LIST_FILE)
	$(chmod 755 $REPO_LIST_FILE)
fi

# Create branches file
if [ ! -f $BRANCHES_FILE ];
then
	echo "Missing temporary file for BRANCHES"
	echo "Creating $BRANCHES_FILE ..."
	$(touch $BRANCHES_FILE)
	$(chmod 755 $BRANCHES_FILE)
fi

# Check if we got a token
if [ -f $TOKEN_FILE ];
then
	TOKEN=$(cat $TOKEN_FILE)
else
	echo "Missing token, getting one..."

	# Request for authorization token from github
	JSON=$(curl -s -u $GITHUB_USERNAME:$GITHUB_PASSWORD -d '{"scopes":["repo"],"note":"Need to create a backup script"}' https://api.github.com/authorizations) 2>&1

	# Save authorizations JSON blob into file
	echo $JSON > $JSON_FILE

	# Break out token from authorizations JSON file
	TOKEN=$(awk -F"[,:]" '{for(i=1;i<=NF;i++){if($i~/\042token\042/){print $(i+1)} } }' $JSON_FILE | sed -e 's/"//g' | sed -e 's/ //g')

	# Save token into file
	echo $TOKEN > $TOKEN_FILE
fi

# At this point we got a token, either from token file, or from github authorizations url

	# Fetch list of repositories
	REPO_LIST=$(curl -s -H "Authorization: token $TOKEN" https://api.github.com/user/repos) 2>&1

	# Save repo list into file
	echo $REPO_LIST > $REPO_LIST_FILE

	REPOS=$(awk -F"[,:]" '{for(i=1;i<=NF;i++){if($i~/\042ssh_url\042/){printf $(i+1) ":" $(i+2)} } }' $REPO_LIST_FILE | sed -e 's/"//g')	

for ssh_url in $REPOS; 
do 
	GITHUB_PROJECT=$(echo $ssh_url | awk -F[/] '{printf $(NF)}')
	BACKUP_SUBDIR=${BACKUP_DIR}${GITHUB_PROJECT}

	echo "---------------------------------------------"
	echo $GITHUB_PROJECT
	echo "---------------------------------------------"
	
	# clone or pull
	if [ ! -d $BACKUP_SUBDIR ]
	then
		$GIT clone $ssh_url $BACKUP_SUBDIR
	fi

	# get branches
	(
		cd $BACKUP_SUBDIR
		BRANCHES=$($GIT branch -a | sed -e 's/\*//g' | awk '{for(i=1;i<=NF;i++) { if($(i) != "" && $(i) != "remotes/origin/HEAD" && $(i) != "->") { print $(i) } } }' | sed -e 's/remotes\///g' | sed -e 's/origin\///g' | sort -u)
		for branch in $BRANCHES
		do
			$GIT checkout $branch
			$GIT pull --rebase
			echo 
		done;
	)

	echo 
done;
