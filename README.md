rvl-github-backup
=================

Backup all your repos and branches.

Get up and running
----
Base64 encode your password

    $ ./genpwd.sh
    Enter your password to get it base64 encoded:<mypassword>
    bXlwYXNzd29yZAo=

Copy/paste your base64 encoded password into rvl-repo-backup.sh:

    PASSWORD_ENCODED=<here>
